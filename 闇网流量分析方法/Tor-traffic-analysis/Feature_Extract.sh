#!/bin/sh
# 给定一个.pcap文件，生成一个pcap同名文件夹并提取特征到文件夹下，然后提取TCP特征到pcap同级文件夹下

folder=$(readlink -f "$1")
echo $folder

filepath=${folder%.*}
filename=${filepath##*/}


cd $T2HOME/tranalyzer2/build/
./tranalyzer -r $folder -w $filepath/${filename} > logfile
cd $T2HOME/scripts/tawk
flowsfile=$filepath/${filename}_flows.txt
tcpfile=${filepath}_TCP.txt
./tawk 'tcp()' $flowsfile > $tcpfile

