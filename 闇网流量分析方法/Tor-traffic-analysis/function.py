# 
from genericpath import isfile
import os,sys,re
import pandas as pd
import glob

numfeatures = ['%dir','flowInd','timeFirst','timeLast','duration','srcIP','srcPort','dstIP','dstPort','dstPortClassN','numPktsSnt','numPktsRcvd','numBytesSnt','numBytesRcvd','minPktSz','maxPktSz','avePktSize','stdPktSize','maxIAT','aveIAT','stdIAT','pktps','bytps','pktAsm','bytAsm','ipMindIPID','ipMaxdIPID','ipMinTTL','ipMaxTTL','ipTTLChg','tcpPSeqCnt','tcpSeqSntBytes','tcpSeqFaultCnt','tcpPAckCnt','tcpFlwLssAckRcvdBytes','tcpAckFaultCnt','tcpInitWinSz','tcpAveWinSz','tcpMinWinSz','tcpMaxWinSz','tcpWinSzDwnCnt','tcpWinSzUpCnt','tcpWinSzChgDirCnt','tcpWinSzThRt','tcpOptPktCnt','tcpOptCnt','tcpMSS','tcpWS','tcpTmS','tcpTmER','tcpBtm','tcpSSASAATrip','tcpRTTAckTripMin','tcpRTTAckTripMax','tcpRTTAckTripAve','tcpRTTAckTripJitAve','tcpRTTSseqAA','tcpRTTAckJitAve','connSip','connDip','connSipDip','connSipDprt','connF','nFpCnt','dsMinPl','dsMaxPl','dsMeanPl','dsLowQuartilePl','dsMedianPl','dsUppQuartilePl','dsIqdPl','dsModePl','dsRangePl','dsStdPl','dsRobStdPl','dsSkewPl','dsExcPl','dsMinIat','dsMaxIat','dsMeanIat','dsLowQuartileIat','dsMedianIat','dsUppQuartileIat','dsIqdIat','dsModeIat','dsRangeIat','dsStdIat','dsRobStdIat','dsSkewIat','dsExcIat','L2L3L4Pl_Iat']

ALL_features = ['timeFirst','timeLast','duration','numHdrDesc','numHdrs','hdrDesc','l4Proto','macStat','macPairs','srcMac_dstMac_numP','dstPortClassN','dstPortClass','numPktsSnt','numPktsRcvd','numBytesSnt','numBytesRcvd','minPktSz','maxPktSz','avePktSize','stdPktSize','minIAT','maxIAT','aveIAT','stdIAT','pktps','bytps','pktAsm','bytAsm','tcpFStat','ipMindIPID','ipMaxdIPID','ipMinTTL','ipMaxTTL','ipTTLChg','ipTOS','ipFlags','ipOptCnt','ipOptCpCl_Num','ip6OptCntHH_D','ip6OptHH_D','tcpISeqN','tcpPSeqCnt','tcpSeqSntBytes','tcpSeqFaultCnt','tcpPAckCnt','tcpFlwLssAckRcvdBytes','tcpAckFaultCnt','tcpInitWinSz','tcpAveWinSz','tcpMinWinSz','tcpMaxWinSz','tcpWinSzDwnCnt','tcpWinSzUpCnt','tcpWinSzChgDirCnt','tcpWinSzThRt','tcpFlags','tcpAnomaly','tcpOptPktCnt','tcpOptCnt','tcpOptions','tcpMSS','tcpWS','tcpMPTBF','tcpMPF','tcpMPAID','tcpMPdssF','tcpTmS','tcpTmER','tcpEcI','tcpUtm','tcpBtm','tcpSSASAATrip','tcpRTTAckTripMin','tcpRTTAckTripMax','tcpRTTAckTripAve','tcpRTTAckTripJitAve','tcpRTTSseqAA','tcpRTTAckJitAve','icmpStat','icmpTCcnt','icmpBFTypH_TypL_Code','icmpTmGtw','icmpEchoSuccRatio','icmpPFindex','connSip','connDip','connSipDip','connSipDprt','connF','nFpCnt','L2L3L4Pl_Iat','tCnt','Ps_Iat_Cnt_PsCnt_IatCnt','dsMinPl','dsMaxPl','dsMeanPl','dsLowQuartilePl','dsMedianPl','dsUppQuartilePl','dsIqdPl','dsModePl','dsRangePl','dsStdPl','dsRobStdPl','dsSkewPl','dsExcPl','dsMinIat','dsMaxIat','dsMeanIat','dsLowQuartileIat','dsMedianIat','dsUppQuartileIat','dsIqdIat','dsModeIat','dsRangeIat','dsStdIat','dsRobStdIat','dsSkewIat','dsExcIat']

AB_features = ['duration', 'srcIP', 'srcPort', 'dstIP', 'dstPort', 'A_timeFirst', 'A_timeLast', 'A_duration', 'A_numHdrDesc', 'A_numHdrs', 'A_hdrDesc', 'A_l4Proto', 'A_macStat', 'A_macPairs', 'A_srcMac_dstMac_numP',  'A_dstPortClassN', 'A_dstPortClass', 'A_numPktsSnt', 'A_numPktsRcvd', 'A_numBytesSnt', 'A_numBytesRcvd', 'A_minPktSz', 'A_maxPktSz', 'A_avePktSize', 'A_stdPktSize', 'A_minIAT', 'A_maxIAT', 'A_aveIAT', 'A_stdIAT', 'A_pktps', 'A_bytps', 'A_pktAsm', 'A_bytAsm', 'A_tcpFStat', 'A_ipMindIPID', 'A_ipMaxdIPID', 'A_ipMinTTL', 'A_ipMaxTTL', 'A_ipTTLChg', 'A_ipTOS', 'A_ipFlags', 'A_ipOptCnt', 'A_ipOptCpCl_Num', 'A_ip6OptCntHH_D', 'A_ip6OptHH_D', 'A_tcpISeqN', 'A_tcpPSeqCnt', 'A_tcpSeqSntBytes', 'A_tcpSeqFaultCnt', 'A_tcpPAckCnt', 'A_tcpFlwLssAckRcvdBytes', 'A_tcpAckFaultCnt', 'A_tcpInitWinSz', 'A_tcpAveWinSz', 'A_tcpMinWinSz', 'A_tcpMaxWinSz', 'A_tcpWinSzDwnCnt', 'A_tcpWinSzUpCnt', 'A_tcpWinSzChgDirCnt', 'A_tcpWinSzThRt', 'A_tcpFlags', 'A_tcpAnomaly', 'A_tcpOptPktCnt', 'A_tcpOptCnt', 'A_tcpOptions', 'A_tcpMSS', 'A_tcpWS', 'A_tcpMPTBF', 'A_tcpMPF', 'A_tcpMPAID', 'A_tcpMPdssF', 'A_tcpTmS', 'A_tcpTmER', 'A_tcpEcI', 'A_tcpUtm', 'A_tcpBtm', 'A_tcpSSASAATrip', 'A_tcpRTTAckTripMin', 'A_tcpRTTAckTripMax', 'A_tcpRTTAckTripAve', 'A_tcpRTTAckTripJitAve', 'A_tcpRTTSseqAA', 'A_tcpRTTAckJitAve', 'A_icmpStat', 'A_icmpTCcnt', 'A_icmpBFTypH_TypL_Code', 'A_icmpTmGtw', 'A_icmpEchoSuccRatio', 'A_icmpPFindex', 'A_connSip', 'A_connDip', 'A_connSipDip', 'A_connSipDprt', 'A_connF', 'A_nFpCnt', 'A_L2L3L4Pl_Iat', 'A_tCnt', 'A_Ps_Iat_Cnt_PsCnt_IatCnt', 'A_dsMinPl', 'A_dsMaxPl', 'A_dsMeanPl', 'A_dsLowQuartilePl', 'A_dsMedianPl', 'A_dsUppQuartilePl', 'A_dsIqdPl', 'A_dsModePl', 'A_dsRangePl', 'A_dsStdPl', 'A_dsRobStdPl', 'A_dsSkewPl', 'A_dsExcPl', 'A_dsMinIat', 'A_dsMaxIat', 'A_dsMeanIat', 'A_dsLowQuartileIat', 'A_dsMedianIat', 'A_dsUppQuartileIat', 'A_dsIqdIat', 'A_dsModeIat', 'A_dsRangeIat', 'A_dsStdIat', 'A_dsRobStdIat', 'A_dsSkewIat', 'A_dsExcIat', 'B_timeFirst', 'B_timeLast', 'B_duration', 'B_numHdrDesc', 'B_numHdrs', 'B_hdrDesc', 'B_l4Proto', 'B_macStat', 'B_macPairs', 'B_srcMac_dstMac_numP', 'B_dstPortClassN', 'B_dstPortClass', 'B_numPktsSnt', 'B_numPktsRcvd', 'B_numBytesSnt', 'B_numBytesRcvd', 'B_minPktSz', 'B_maxPktSz', 'B_avePktSize', 'B_stdPktSize', 'B_minIAT', 'B_maxIAT', 'B_aveIAT', 'B_stdIAT', 'B_pktps', 'B_bytps', 'B_pktAsm', 'B_bytAsm', 'B_tcpFStat', 'B_ipMindIPID', 'B_ipMaxdIPID', 'B_ipMinTTL', 'B_ipMaxTTL', 'B_ipTTLChg', 'B_ipTOS', 'B_ipFlags', 'B_ipOptCnt', 'B_ipOptCpCl_Num', 'B_ip6OptCntHH_D', 'B_ip6OptHH_D', 'B_tcpISeqN', 'B_tcpPSeqCnt', 'B_tcpSeqSntBytes', 'B_tcpSeqFaultCnt', 'B_tcpPAckCnt', 'B_tcpFlwLssAckRcvdBytes', 'B_tcpAckFaultCnt', 'B_tcpInitWinSz', 'B_tcpAveWinSz', 'B_tcpMinWinSz', 'B_tcpMaxWinSz', 'B_tcpWinSzDwnCnt', 'B_tcpWinSzUpCnt', 'B_tcpWinSzChgDirCnt', 'B_tcpWinSzThRt', 'B_tcpFlags', 'B_tcpAnomaly', 'B_tcpOptPktCnt', 'B_tcpOptCnt', 'B_tcpOptions', 'B_tcpMSS', 'B_tcpWS', 'B_tcpMPTBF', 'B_tcpMPF', 'B_tcpMPAID', 'B_tcpMPdssF', 'B_tcpTmS', 'B_tcpTmER', 'B_tcpEcI', 'B_tcpUtm', 'B_tcpBtm', 'B_tcpSSASAATrip', 'B_tcpRTTAckTripMin', 'B_tcpRTTAckTripMax', 'B_tcpRTTAckTripAve', 'B_tcpRTTAckTripJitAve', 'B_tcpRTTSseqAA', 'B_tcpRTTAckJitAve', 'B_icmpStat', 'B_icmpTCcnt', 'B_icmpBFTypH_TypL_Code', 'B_icmpTmGtw', 'B_icmpEchoSuccRatio', 'B_icmpPFindex', 'B_connSip', 'B_connDip', 'B_connSipDip', 'B_connSipDprt', 'B_connF', 'B_nFpCnt', 'B_L2L3L4Pl_Iat', 'B_tCnt', 'B_Ps_Iat_Cnt_PsCnt_IatCnt', 'B_dsMinPl', 'B_dsMaxPl', 'B_dsMeanPl', 'B_dsLowQuartilePl', 'B_dsMedianPl', 'B_dsUppQuartilePl', 'B_dsIqdPl', 'B_dsModePl', 'B_dsRangePl', 'B_dsStdPl', 'B_dsRobStdPl', 'B_dsSkewPl', 'B_dsExcPl', 'B_dsMinIat', 'B_dsMaxIat', 'B_dsMeanIat', 'B_dsLowQuartileIat', 'B_dsMedianIat', 'B_dsUppQuartileIat', 'B_dsIqdIat', 'B_dsModeIat', 'B_dsRangeIat', 'B_dsStdIat', 'B_dsRobStdIat', 'B_dsSkewIat', 'B_dsExcIat']

pl_iat = ['0A_PL', '0A_IAT', '1A_PL', '1A_IAT', '2A_PL', '2A_IAT', '3A_PL', '3A_IAT', '4A_PL', '4A_IAT', '5A_PL', '5A_IAT', '6A_PL', '6A_IAT', '7A_PL', '7A_IAT', '8A_PL', '8A_IAT', '9A_PL', '9A_IAT', '10A_PL', '10A_IAT', '11A_PL', '11A_IAT', '12A_PL', '12A_IAT', '13A_PL', '13A_IAT', '14A_PL', '14A_IAT', '15A_PL', '15A_IAT', '16A_PL', '16A_IAT', '17A_PL', '17A_IAT', '18A_PL', '18A_IAT', '19A_PL', '19A_IAT', '0B_PL', '0B_IAT', '1B_PL', '1B_IAT', '2B_PL', '2B_IAT', '3B_PL', '3B_IAT', '4B_PL', '4B_IAT', '5B_PL', '5B_IAT', '6B_PL', '6B_IAT', '7B_PL', '7B_IAT', '8B_PL', '8B_IAT', '9B_PL', '9B_IAT', '10B_PL', '10B_IAT', '11B_PL', '11B_IAT', '12B_PL', '12B_IAT', '13B_PL', '13B_IAT', '14B_PL', '14B_IAT', '15B_PL', '15B_IAT', '16B_PL', '16B_IAT', '17B_PL', '17B_IAT', '18B_PL', '18B_IAT', '19B_PL', '19B_IAT']

columns = ['duration', 'srcIP', 'srcPort', 'dstIP', 'dstPort', 'A_timeFirst', 'A_timeLast', 'A_duration', 'A_numHdrDesc', 'A_numHdrs', 'A_hdrDesc', 'A_l4Proto', 'A_macStat', 'A_macPairs', 'A_srcMac_dstMac_numP',  'A_dstPortClassN', 'A_dstPortClass', 'A_numPktsSnt', 'A_numPktsRcvd', 'A_numBytesSnt', 'A_numBytesRcvd', 'A_minPktSz', 'A_maxPktSz', 'A_avePktSize', 'A_stdPktSize', 'A_minIAT', 'A_maxIAT', 'A_aveIAT', 'A_stdIAT', 'A_pktps', 'A_bytps', 'A_pktAsm', 'A_bytAsm', 'A_tcpFStat', 'A_ipMindIPID', 'A_ipMaxdIPID', 'A_ipMinTTL', 'A_ipMaxTTL', 'A_ipTTLChg', 'A_ipTOS', 'A_ipFlags', 'A_ipOptCnt', 'A_ipOptCpCl_Num', 'A_ip6OptCntHH_D', 'A_ip6OptHH_D', 'A_tcpISeqN', 'A_tcpPSeqCnt', 'A_tcpSeqSntBytes', 'A_tcpSeqFaultCnt', 'A_tcpPAckCnt', 'A_tcpFlwLssAckRcvdBytes', 'A_tcpAckFaultCnt', 'A_tcpInitWinSz', 'A_tcpAveWinSz', 'A_tcpMinWinSz', 'A_tcpMaxWinSz', 'A_tcpWinSzDwnCnt', 'A_tcpWinSzUpCnt', 'A_tcpWinSzChgDirCnt', 'A_tcpWinSzThRt', 'A_tcpFlags', 'A_tcpAnomaly', 'A_tcpOptPktCnt', 'A_tcpOptCnt', 'A_tcpOptions', 'A_tcpMSS', 'A_tcpWS', 'A_tcpMPTBF', 'A_tcpMPF', 'A_tcpMPAID', 'A_tcpMPdssF', 'A_tcpTmS', 'A_tcpTmER', 'A_tcpEcI', 'A_tcpUtm', 'A_tcpBtm', 'A_tcpSSASAATrip', 'A_tcpRTTAckTripMin', 'A_tcpRTTAckTripMax', 'A_tcpRTTAckTripAve', 'A_tcpRTTAckTripJitAve', 'A_tcpRTTSseqAA', 'A_tcpRTTAckJitAve', 'A_icmpStat', 'A_icmpTCcnt', 'A_icmpBFTypH_TypL_Code', 'A_icmpTmGtw', 'A_icmpEchoSuccRatio', 'A_icmpPFindex', 'A_connSip', 'A_connDip', 'A_connSipDip', 'A_connSipDprt', 'A_connF', 'A_nFpCnt', 'A_L2L3L4Pl_Iat', 'A_tCnt', 'A_Ps_Iat_Cnt_PsCnt_IatCnt', 'A_dsMinPl', 'A_dsMaxPl', 'A_dsMeanPl', 'A_dsLowQuartilePl', 'A_dsMedianPl', 'A_dsUppQuartilePl', 'A_dsIqdPl', 'A_dsModePl', 'A_dsRangePl', 'A_dsStdPl', 'A_dsRobStdPl', 'A_dsSkewPl', 'A_dsExcPl', 'A_dsMinIat', 'A_dsMaxIat', 'A_dsMeanIat', 'A_dsLowQuartileIat', 'A_dsMedianIat', 'A_dsUppQuartileIat', 'A_dsIqdIat', 'A_dsModeIat', 'A_dsRangeIat', 'A_dsStdIat', 'A_dsRobStdIat', 'A_dsSkewIat', 'A_dsExcIat', 'B_timeFirst', 'B_timeLast', 'B_duration', 'B_numHdrDesc', 'B_numHdrs', 'B_hdrDesc', 'B_l4Proto', 'B_macStat', 'B_macPairs', 'B_srcMac_dstMac_numP', 'B_dstPortClassN', 'B_dstPortClass', 'B_numPktsSnt', 'B_numPktsRcvd', 'B_numBytesSnt', 'B_numBytesRcvd', 'B_minPktSz', 'B_maxPktSz', 'B_avePktSize', 'B_stdPktSize', 'B_minIAT', 'B_maxIAT', 'B_aveIAT', 'B_stdIAT', 'B_pktps', 'B_bytps', 'B_pktAsm', 'B_bytAsm', 'B_tcpFStat', 'B_ipMindIPID', 'B_ipMaxdIPID', 'B_ipMinTTL', 'B_ipMaxTTL', 'B_ipTTLChg', 'B_ipTOS', 'B_ipFlags', 'B_ipOptCnt', 'B_ipOptCpCl_Num', 'B_ip6OptCntHH_D', 'B_ip6OptHH_D', 'B_tcpISeqN', 'B_tcpPSeqCnt', 'B_tcpSeqSntBytes', 'B_tcpSeqFaultCnt', 'B_tcpPAckCnt', 'B_tcpFlwLssAckRcvdBytes', 'B_tcpAckFaultCnt', 'B_tcpInitWinSz', 'B_tcpAveWinSz', 'B_tcpMinWinSz', 'B_tcpMaxWinSz', 'B_tcpWinSzDwnCnt', 'B_tcpWinSzUpCnt', 'B_tcpWinSzChgDirCnt', 'B_tcpWinSzThRt', 'B_tcpFlags', 'B_tcpAnomaly', 'B_tcpOptPktCnt', 'B_tcpOptCnt', 'B_tcpOptions', 'B_tcpMSS', 'B_tcpWS', 'B_tcpMPTBF', 'B_tcpMPF', 'B_tcpMPAID', 'B_tcpMPdssF', 'B_tcpTmS', 'B_tcpTmER', 'B_tcpEcI', 'B_tcpUtm', 'B_tcpBtm', 'B_tcpSSASAATrip', 'B_tcpRTTAckTripMin', 'B_tcpRTTAckTripMax', 'B_tcpRTTAckTripAve', 'B_tcpRTTAckTripJitAve', 'B_tcpRTTSseqAA', 'B_tcpRTTAckJitAve', 'B_icmpStat', 'B_icmpTCcnt', 'B_icmpBFTypH_TypL_Code', 'B_icmpTmGtw', 'B_icmpEchoSuccRatio', 'B_icmpPFindex', 'B_connSip', 'B_connDip', 'B_connSipDip', 'B_connSipDprt', 'B_connF', 'B_nFpCnt', 'B_L2L3L4Pl_Iat', 'B_tCnt', 'B_Ps_Iat_Cnt_PsCnt_IatCnt', 'B_dsMinPl', 'B_dsMaxPl', 'B_dsMeanPl', 'B_dsLowQuartilePl', 'B_dsMedianPl', 'B_dsUppQuartilePl', 'B_dsIqdPl', 'B_dsModePl', 'B_dsRangePl', 'B_dsStdPl', 'B_dsRobStdPl', 'B_dsSkewPl', 'B_dsExcPl', 'B_dsMinIat', 'B_dsMaxIat', 'B_dsMeanIat', 'B_dsLowQuartileIat', 'B_dsMedianIat', 'B_dsUppQuartileIat', 'B_dsIqdIat', 'B_dsModeIat', 'B_dsRangeIat', 'B_dsStdIat', 'B_dsRobStdIat', 'B_dsSkewIat', 'B_dsExcIat','0A_PL', '0A_IAT', '1A_PL', '1A_IAT', '2A_PL', '2A_IAT', '3A_PL', '3A_IAT', '4A_PL', '4A_IAT', '5A_PL', '5A_IAT', '6A_PL', '6A_IAT', '7A_PL', '7A_IAT', '8A_PL', '8A_IAT', '9A_PL', '9A_IAT', '10A_PL', '10A_IAT', '11A_PL', '11A_IAT', '12A_PL', '12A_IAT', '13A_PL', '13A_IAT', '14A_PL', '14A_IAT', '15A_PL', '15A_IAT', '16A_PL', '16A_IAT', '17A_PL', '17A_IAT', '18A_PL', '18A_IAT', '19A_PL', '19A_IAT', '0B_PL', '0B_IAT', '1B_PL', '1B_IAT', '2B_PL', '2B_IAT', '3B_PL', '3B_IAT', '4B_PL', '4B_IAT', '5B_PL', '5B_IAT', '6B_PL', '6B_IAT', '7B_PL', '7B_IAT', '8B_PL', '8B_IAT', '9B_PL', '9B_IAT', '10B_PL', '10B_IAT', '11B_PL', '11B_IAT', '12B_PL', '12B_IAT', '13B_PL', '13B_IAT', '14B_PL', '14B_IAT', '15B_PL', '15B_IAT', '16B_PL', '16B_IAT', '17B_PL', '17B_IAT', '18B_PL', '18B_IAT', '19B_PL', '19B_IAT']

numerical = ['duration','dstPortClassN','numPktsSnt','numPktsRcvd','numBytesSnt','numBytesRcvd','minPktSz','maxPktSz','avePktSize','stdPktSize','maxIAT','aveIAT','stdIAT','pktps','bytps','pktAsm','bytAsm','ipMindIPID','ipMaxdIPID','ipMinTTL','ipMaxTTL','ipTTLChg','tcpPSeqCnt','tcpSeqSntBytes','tcpSeqFaultCnt','tcpPAckCnt','tcpFlwLssAckRcvdBytes','tcpAckFaultCnt','tcpInitWinSz','tcpAveWinSz','tcpMinWinSz','tcpMaxWinSz','tcpWinSzDwnCnt','tcpWinSzUpCnt','tcpWinSzChgDirCnt','tcpWinSzThRt','tcpOptPktCnt','tcpOptCnt','tcpMSS','tcpWS','tcpTmS','tcpTmER','tcpBtm','tcpSSASAATrip','tcpRTTAckTripMin','tcpRTTAckTripMax','tcpRTTAckTripAve','tcpRTTAckTripJitAve','tcpRTTSseqAA','tcpRTTAckJitAve','connSip','connDip','connSipDip','connSipDprt','connF','nFpCnt','dsMinPl','dsMaxPl','dsMeanPl','dsLowQuartilePl','dsMedianPl','dsUppQuartilePl','dsIqdPl','dsModePl','dsRangePl','dsStdPl','dsRobStdPl','dsSkewPl','dsExcPl','dsMinIat','dsMaxIat','dsMeanIat','dsLowQuartileIat','dsMedianIat','dsUppQuartileIat','dsIqdIat','dsModeIat','dsRangeIat','dsStdIat','dsRobStdIat','dsSkewIat','dsExcIat']

AB_num = ['duration','A_duration','A_dstPortClassN', 'A_numPktsSnt', 'A_numPktsRcvd', 'A_numBytesSnt', 'A_numBytesRcvd', 'A_minPktSz', 'A_maxPktSz', 'A_avePktSize', 'A_stdPktSize', 'A_maxIAT', 'A_aveIAT', 'A_stdIAT', 'A_pktps', 'A_bytps', 'A_pktAsm', 'A_bytAsm', 'A_ipMindIPID', 'A_ipMaxdIPID', 'A_ipMinTTL', 'A_ipMaxTTL', 'A_ipTTLChg', 'A_tcpPSeqCnt', 'A_tcpSeqSntBytes', 'A_tcpSeqFaultCnt', 'A_tcpPAckCnt', 'A_tcpFlwLssAckRcvdBytes', 'A_tcpAckFaultCnt', 'A_tcpInitWinSz', 'A_tcpAveWinSz', 'A_tcpMinWinSz', 'A_tcpMaxWinSz', 'A_tcpWinSzDwnCnt', 'A_tcpWinSzUpCnt', 'A_tcpWinSzChgDirCnt', 'A_tcpWinSzThRt', 'A_tcpOptPktCnt', 'A_tcpOptCnt', 'A_tcpMSS', 'A_tcpWS', 'A_tcpTmS', 'A_tcpTmER', 'A_tcpBtm', 'A_tcpSSASAATrip', 'A_tcpRTTAckTripMin', 'A_tcpRTTAckTripMax', 'A_tcpRTTAckTripAve', 'A_tcpRTTAckTripJitAve', 'A_tcpRTTSseqAA', 'A_tcpRTTAckJitAve', 'A_connSip', 'A_connDip', 'A_connSipDip', 'A_connSipDprt', 'A_connF', 'A_nFpCnt', 'A_dsMinPl', 'A_dsMaxPl', 'A_dsMeanPl', 'A_dsLowQuartilePl', 'A_dsMedianPl', 'A_dsUppQuartilePl', 'A_dsIqdPl', 'A_dsModePl', 'A_dsRangePl', 'A_dsStdPl', 'A_dsRobStdPl', 'A_dsSkewPl', 'A_dsExcPl', 'A_dsMinIat', 'A_dsMaxIat', 'A_dsMeanIat', 'A_dsLowQuartileIat', 'A_dsMedianIat', 'A_dsUppQuartileIat', 'A_dsIqdIat', 'A_dsModeIat', 'A_dsRangeIat', 'A_dsStdIat', 'A_dsRobStdIat', 'A_dsSkewIat', 'A_dsExcIat', 'B_duration', 'B_dstPortClassN', 'B_numPktsSnt', 'B_numPktsRcvd', 'B_numBytesSnt', 'B_numBytesRcvd', 'B_minPktSz', 'B_maxPktSz', 'B_avePktSize', 'B_stdPktSize', 'B_maxIAT', 'B_aveIAT', 'B_stdIAT', 'B_pktps', 'B_bytps', 'B_pktAsm', 'B_bytAsm', 'B_ipMindIPID', 'B_ipMaxdIPID', 'B_ipMinTTL', 'B_ipMaxTTL', 'B_ipTTLChg', 'B_tcpPSeqCnt', 'B_tcpSeqSntBytes', 'B_tcpSeqFaultCnt', 'B_tcpPAckCnt', 'B_tcpFlwLssAckRcvdBytes', 'B_tcpAckFaultCnt', 'B_tcpInitWinSz', 'B_tcpAveWinSz', 'B_tcpMinWinSz', 'B_tcpMaxWinSz', 'B_tcpWinSzDwnCnt', 'B_tcpWinSzUpCnt', 'B_tcpWinSzChgDirCnt', 'B_tcpWinSzThRt', 'B_tcpOptPktCnt', 'B_tcpOptCnt', 'B_tcpMSS', 'B_tcpWS', 'B_tcpTmS', 'B_tcpTmER', 'B_tcpBtm', 'B_tcpSSASAATrip', 'B_tcpRTTAckTripMin', 'B_tcpRTTAckTripMax', 'B_tcpRTTAckTripAve', 'B_tcpRTTAckTripJitAve', 'B_tcpRTTSseqAA', 'B_tcpRTTAckJitAve', 'B_connSip', 'B_connDip', 'B_connSipDip', 'B_connSipDprt', 'B_connF', 'B_nFpCnt', 'B_dsMinPl', 'B_dsMaxPl', 'B_dsMeanPl', 'B_dsLowQuartilePl', 'B_dsMedianPl', 'B_dsUppQuartilePl', 'B_dsIqdPl', 'B_dsModePl', 'B_dsRangePl', 'B_dsStdPl', 'B_dsRobStdPl', 'B_dsSkewPl', 'B_dsExcPl', 'B_dsMinIat', 'B_dsMaxIat', 'B_dsMeanIat', 'B_dsLowQuartileIat', 'B_dsMedianIat', 'B_dsUppQuartileIat', 'B_dsIqdIat', 'B_dsModeIat', 'B_dsRangeIat', 'B_dsStdIat', 'B_dsRobStdIat', 'B_dsSkewIat', 'B_dsExcIat']


def tree(path):
    # 深度遍历目录，生成一个迭代器
    for root,dirs,files in os.walk(path,topdown=False):
        for dir in dirs:
            yield os.path.join(root,dir)


'''
while(True):
    try:
        print(next(f))
    except StopIteration:
        break
'''
'''
for name in f:
    print(name)
'''

def merge_two_direction_data(data):
    """合并双向流特征"""
    data['%dir']='A<->B'
    choose_sum_features=['duration']
    choose_min_features=[feature for feature in numerical if re.search('min|Min',feature)]
    choose_min_features.extend([])
    choose_max_features=[feature for feature in numerical if re.search('max|Max',feature)]
    choose_max_features.extend([])
    choose_avg_features=[feature for feature in numerical if re.search('ave|Ave|Mean',feature)]
    choose_avg_features.extend([])
    # print(choose_min_features)
    # print(choose_max_features)
    # print(choose_avg_features)
    nums_row=len(data)
    select_index=[]
    i=0
    while i<nums_row-1:
        if data.loc[i,'flowInd'] != data.loc[i+1,'flowInd']: # 单向流
            data.loc[i,'%dir'] = 'A'
            select_index.append(i)
            i+=1
            continue
        for feature in choose_sum_features:
            data.loc[i,feature]+=data.loc[i+1,feature]
        for feature in choose_min_features:
            data.loc[i,feature]=min(data.loc[i,feature],data.loc[i+1,feature])
        for feature in choose_max_features:
            data.loc[i,feature]=max(data.loc[i,feature],data.loc[i+1,feature])
        for feature in choose_avg_features:
            data.loc[i,feature]=(data.loc[i,feature]+data.loc[i+1,feature])/2
        select_index.append(i)
        i+=2
    return data[data.index.isin(select_index)]

def merge_features(file):
    # 给定一个txt特征文件，将其中的AB流特征合并，以及提取出AB流的前20包的包长和时间间隔特征，保存为同名csv文件
    data = pd.read_csv(file, low_memory=False,sep='\t')
    #data = merge_two_direction_data(data)
    
    features = pd.DataFrame(columns=columns)

    for n in range(1,data['flowInd'].max()+1):
        if(len(data[data['flowInd']==n])!=2):
            continue
        feature = []

        A = data[(data['flowInd']==n)&(data['%dir']=='A')]
        B = data[(data['flowInd']==n)&(data['%dir']=='B')]
        feature.append(max(A['timeLast'].max(),B['timeLast'].max())-min(A['timeFirst'].max(),B['timeFirst'].max()))
        feature.extend(A[['srcIP', 'srcPort', 'dstIP', 'dstPort']].values.tolist()[0])
        feature.extend(A[ALL_features].values.tolist()[0])
        feature.extend(B[ALL_features].values.tolist()[0])

        PlIat = re.split('_|;',A['L2L3L4Pl_Iat'].values.tolist()[0])
        while(len(PlIat)<40): PlIat.append(' ')
        feature.extend(PlIat)
        PlIat = re.split('_|;',B['L2L3L4Pl_Iat'].values.tolist()[0])
        while(len(PlIat)<40): PlIat.append(' ')
        feature.extend(PlIat)

        features.loc[n-1] = feature

    features=features.drop(['A_L2L3L4Pl_Iat','B_L2L3L4Pl_Iat'], axis=1)
    dst = os.path.splitext(file)[0]+'.csv'
    features.to_csv(dst,index=False, sep=',')

def aggregation_csv(filepath, suffix):
    # 给定一个文件夹，将文件夹下所有csv文件合并，保存到与该文件夹同级的同名csv文件中
    #csv_list = glob.glob(filepath+'/*.csv')
    # suffix='/**/*'，深度遍历所有csv文件
    # suffix='/*'，只对当前目录下csv
    if(os.path.isdir(filepath)):
        csv_list = glob.glob(filepath+suffix+'.csv', recursive=True)
    elif(os.path.isfile(filepath)):
        with open(filepath, 'r') as f:
            csv_list = f.read().splitlines()
            filepath = os.path.splitext(filepath)[0]
    if(len(csv_list)<1):
        return False
    df1=pd.read_csv(csv_list[0],low_memory=False,sep=',')
    for i in range(1,len(csv_list)):
        df2 = pd.read_csv(csv_list[i],low_memory=False,sep=',')
        df1 = pd.concat([df1,df2], axis=0)
    if((filepath[-1]=='/') | (filepath[-1]=='\\')):
        filepath=filepath[:-1]
    df1.replace(' ', 0)
    df1.to_csv(filepath+'.csv', index=False, sep=',')
    return True

def ADD_label(file, classname):
    data = pd.read_csv(file, low_memory=False, sep=',')
    last_feature = data.columns[len(data.columns)-1]
    if(re.match(r'class[0-9]', last_feature)):
        column = 'class'+str(int(last_feature[5])+1)
    else:
        column = 'class1'
    data.loc[:, column]=classname
    data.to_csv(file, index=False, sep=',')
