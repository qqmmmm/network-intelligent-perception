# Collecting VOIP traffic

VOIP软件： WeChat

1. Call_Wechat.py：用于生成CallWechat类，定义初始化一些基本的变量，以及进行VOIP通话时会用到的自动化软件调用方法
2. Sniff.py：用于对网关以及本机流量的搜集，通过实例化CallWechat类来进行VOIP通话操作
3. SSHConnection.py：定义SSH类，实现与网关的上传下载以及执行shell命令的交互



代理工具：

> Proxycap：将wechat应用进程产生的流量转到到指定的代理上



