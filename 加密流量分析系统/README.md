# 加密流量分析系统

#### 介绍
​		本仓库为网络空间安全学院程光教授课题组加密流量方向研究内容，主要实现了一种加密流量分析原型系统，具有可扩展性。系统整合了该课题组产出的若干最新研究成果，系统包含流量pcap数据上传、加密流量过滤及特征提取、基于多模型的加密流量应用分类等功能，目前本系统作为样例进行展示，后续将持续添加新的研究成果和功能。

​		`系统总览`模块以图形化统计数据为用户展示了本系统的总体结构和功能模块。

![1](imgs/1.jpg)

​		`数据及处理`模块支持用户上传原始pcap文件，经过后端模块处理可获得五元组流的长度序列特征，并保存于csv文件，用户可根据需要自主选择特征文件进行下载。特征文件主要为后续加密流量分析功能提供有效特征支持。

![4](imgs/4.jpg)

​		`加密流量应用分类`模块集成了三种先进的加密流量分类模型，分别是FS-Net、LS-LSTM和LS-CapsNet模型，该模块中包含了对三种模型的介绍和原理示意图，用户可选择多种协议单元（如TLS报文、TCP报文）实现对不同粒度数据类型的应用分类。同时，模块还支持单例验证模式和对比验证模式，以实现对单一模型分类功能的分析或多模型分类功能的对比。

![2](imgs/2.jpg)

![3](imgs/3.jpg)

#### 软件架构
目前包括系统模块总览、数据及处理、加密流量应用分类四个功能模块


#### 安装教程 & 使用说明

系统为前后端分离的开发模式，后端代码以java语言实现，存储于`sys_back`，前端代码以vue+elementui套件实现，存储于`sys_front`。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
