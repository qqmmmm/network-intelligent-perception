#include <cmath>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <map>
#include <fstream>
#include "BOBHASH32.h"
#include "ssummary.h"
#include "CountMinSketch.h"

using namespace std;
map <string ,int> B,C;
struct node {string x;int y;} p[33000005];
//ifstream fin("D:\\文档\\文件\\20221101 TopK\\Data\\CERNET60",ios::in|ios::binary);
ifstream fin("M:\\CAIDA2018.dat",ios::in|ios::binary);
//ifstream fin("C:\\Users\\Password\\Desktop\\HK\\campus.dat",ios::in|ios::binary);


void str2info(string x) {
    unsigned char info[13] = { 0 };
    memcpy(info, x.c_str(), x.length());

    char src_ip[20] = { 0 };
    sprintf(src_ip, "%u.%u.%u.%u", info[3], info[2], info[1], info[0]);

    char dst_ip[20] = { 0 };
    sprintf(dst_ip, "%u.%u.%u.%u", info[7], info[6], info[5], info[4]);

    char src_port[10] = { 0 };
    sprintf(src_port, "%u", (info[9] << 8) + info[8]);

    char dst_port[10] = { 0 };
    sprintf(dst_port, "%u", (info[11] << 8) + info[10]);

    printf("%-16s| %-16s| %-8s| %-8s| %-6u| ", src_ip, dst_ip, src_port, dst_port, info[12]);
}


char a[105];
string Read()
{
    fin.read(a,13);
    a[13]='\0';
    //string tmp=a;
    return a;
}
int cmp(node i,node j) {return i.y>j.y;}
int main()
{
    int MEM,K;
    cout<<"MEM(KB)=";
    cin>>MEM;
    cout<<"Top: ";
    cin>>K;

    int num_flows;
    //int m;
    cout<<"Packets(X 1W):";
    cin>>num_flows;

    int m = num_flows*10000;
    cout<<"the number of Packets = "<<m<<endl;


    int cm_M;
    //for (cm_M = 1; 32*cm_M*CMS_d+432*K<= MEM*1024*8; cm_M++);
    for (cm_M = 1; 32*cm_M*CMS_d+208*K<= MEM*1024*8; cm_M++);
    MyCountMinSketch* cms; cms = new MyCountMinSketch(cm_M, K);
    //cout<<"M2_CM-sketch="<<cm_M<<endl<<endl;



    // Inserting
    for (int i=1; i<=m; i++)
	{
	    if (i%(m/10)==0) cout<<"Insert "<<i<<endl;
		string s=Read();
		B[s]++;
        cms->Insert(s);

	}

    cms->work();

    cout<<"preparing true flow"<<endl;
	// preparing true flow
	int cnt=0;
    for (map <string,int>::iterator sit=B.begin(); sit!=B.end(); sit++)
    {
        p[++cnt].x=sit->first;
        p[cnt].y=sit->second;
    }
    sort(p+1,p+cnt+1,cmp);
    for (int i=1; i<=K; i++) C[p[i].x]=p[i].y;

    // Calculating PRE, ARE, AAE
    cout<<"Calculating"<<endl;

    int cm_sum = 0, cm_AAE = 0; double cm_ARE = 0;
    string cm_string; int cm_num;
    printf("\n");
    printf("CM-Sketch:\n");
    printf("%-16s| %-16s| %-8s| %-8s| %-6s| %-8s|\n", "源IP ", "目的IP ", "源端口 ", "目的端口 ", "协议号 ", "真实值 ");
    for (int i = 0; i < K; i++)
    {
        cm_string = (cms->Query(i)).first; cm_num = (cms->Query(i)).second;
        cm_AAE += abs(B[cm_string] - cm_num); cm_ARE += abs(B[cm_string] - cm_num) / (B[cm_string] + 0.0);
        if (C[cm_string]) cm_sum++;
        str2info(cm_string);
        printf("%-8d|\n", cm_num);
    }
    printf("\n\n");

    printf("CountMin-Sketch:\nAccepted: %d/%d  %.10f\nARE: %.10f\nAAE: %.10f\n\n", cm_sum, K, (cm_sum / (K + 0.0)), cm_ARE / K, cm_AAE / (K + 0.0));

    system("pause");
    return 0;
}
