#include "function.h"

// TrafficLabelling/Monday-WorkingHours.pcap_ISCX.csv

int main(){
    unsigned int bitnum=1;
    BitMap *t;
    BMInit(t);//初始化
    FILE *f;
    fpos_t header;
    char file[100];
    double v;//采样因子
    unsigned int count;
    unsigned int result;
    unsigned int hspace;

    int s=1;
    while(s){
        printf("---------------------------------------------------------------------------\n");
        printf("选择算法：1.直接位图算法  2.虚拟位图算法  0.退出程序\n");
        scanf("%d",&s);
        switch(s){
            case 1://直接位图算法
                printf("输入文件路径：");
                scanf("%s",file);
                f=fopen(file,"r");
                if(f!=NULL){
                    fgetpos(f, &header);//记录文件启示位置
                    hspace=Hspace(f);
                    bitnum=hspace;
                    BMCreat(t,bitnum);///确保每次循环位图为全0
                    fsetpos(f, &header);
                    count=BM_COUNT(t,f,hspace);
                    result=Result(count,t->size);
                    printf("流数量估计值为%u\n",result);
                    fclose(f);
                }
                else
                    printf("文件打开失败，请检查文件路径\n");
            break;

            case 2://虚拟位图算法
                printf("输入文件路径：");
                scanf("%s",file);
                f=fopen(file,"r");
                if(f!=NULL){
                    fgetpos(f, &header);//记录文件启示位置
                    hspace=Hspace(f);
                    printf("输入采样因子v：");
                    scanf("%lf",&v);
                    bitnum=hspace*v;//确定虚拟位图大小
                    BMCreat(t,bitnum);///确保每次循环位图为全0
                    fsetpos(f, &header);
                    count=BM_COUNT(t,f,hspace);
                    result=Result(count,t->size)/v;
                    printf("流数量估计值为%u\n",result);
                    fclose(f);
                }
                else
                    printf("文件打开失败，请检查文件路径\n");
            break;

            case 0:
                printf("程序已结束！\n");
                printf("---------------------------------------------------------------------------\n");
            break;

            default:
            break;
        }
        //BMZero(t);
    }
    BMDestroy(t);
    return 0;
}