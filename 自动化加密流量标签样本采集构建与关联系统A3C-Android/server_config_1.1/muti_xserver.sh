#!/usr/bin/env bash
# 本文件用于批量创建虚拟接口，并自动配置隧道
# 需要root权限，使用bash命令执行此文件

# 检测执行用户权限是否为root
if [ $UID != 0 ]
then
    echo "错误！未使用root权限执行！"
    exit 0
fi

file_path="./virtual_interface.csv"  # 虚拟接口配置文件
log_path="./log/"  # 日志目录
add_pre="10.0.0."  # 虚拟地址前缀
tun_pre="tun"  # 虚拟接口名称前缀
dns_addr="114.114.114.114"  # dns地址
password="test"  # 隧道密钥

# 检测配置文件是否存在，不存在则创建
if [ ! -f ${file_path} ];then
    > ${file_path}
    echo "文件创建成功"
fi

# 检测日志目录是否存在，不存在则创建
if [ ! -d ${log_path} ];then
    mkdir -p ${log_path}
fi

# 读取文件已有行数
int=$(awk 'END {print NR}' ${file_path})
echo "目前已开启接口数："${int}
int=$((${int}+1))

echo -n  "请输入您想要新建的虚拟接口数量："
read number

# 生成并写入虚拟接口配置列表
if ((${number}>=1))
then
    i=1
    while(( ${i} <= ${number} ))
    do
        end=$((${int}*2))
        start=$((${end}-1))
        tun_name=${tun_pre}${int}  #虚拟接口名称
        client_addr=${add_pre}${start}  #客户端虚拟地址
        server_addr=${add_pre}${end}  #服务器虚拟地址
        # 创建虚拟接口
        ip tuntap add dev ${tun_name} mode tun
        # 服务器与客户端配置地址
        ifconfig ${tun_name} ${server_addr} dstaddr ${client_addr} up
        # 日志文件
        log_time=$(date "+%Y-%m-%d %H:%M:%S")
        log_name=${log_path}${tun_name}".txt"
        echo "TUN接口名称，客户端地址，服务器地址" >> ${log_name}
        echo ${tun_name},${client_addr},${server_addr} >> ${log_name}
        echo ${log_time}"  TUN接口开启" >> ${log_name}
        # 开启隧道及程序（后台运行，并记录进程号）
        pid_num=$(nohup ./ToyVpnServer ${tun_name} 8000 ${password} \
            -m 1400 -a ${client_addr} 32 -d ${dns_addr} -r 0.0.0.0 0 \
            >>${log_name} & echo $!)
        # 在文件中追加更新虚拟接口配置列表
        echo ${tun_name},${pid_num},${client_addr},${server_addr} >> ${file_path}
        let "i++"
        let "int++"
    done
    echo "已开启虚拟接口数："$((${int}-1))
else
    echo "输入参数需大于0"
fi