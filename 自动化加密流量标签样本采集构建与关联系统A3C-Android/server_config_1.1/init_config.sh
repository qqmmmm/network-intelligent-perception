#!/usr/bin/env bash
# 本文件用于对服务器环境进行初始化配置（开关机后需重新配置）
# 需要root权限，使用bash命令执行此文件

# 检测执行用户权限是否为root
if [ $UID != 0 ]
then
    echo "错误！未使用root权限执行！"
    exit 0
fi

# 1.使用make编译ToyVpnServer.cpp
make
# 2.开启ip转发功能
sysctl -w net.ipv4.ip_forward=1
sysctl -p
# 3.在物理网卡上进行NAT；10.0.0.0/8为使用VPN的设备源地址集合
    # 读取物理网卡名称（默认为第一个，可通过ifconfig查看）
net_name=$(ls /sys/class/net | head -1)
iptables -t nat -A POSTROUTING -s 10.0.0.0/8 -o ${net_name} -j MASQUERADE
# 当需要翻墙时，删除上一行，并使用tran.sh和V2Ray