#!/bin/bash
cat /dev/null > /etc/ssh/log/watch.log
CHECKDIR="/home/ygu/test"
LOG="/home/ygu/watch.log"
inotifywait -mrq --timefmt '%y-%m-%d %H:%M:%S' --format '%T %w %f %e' -e 'create,delete,modify,attrib,move' $CHECKDIR >> /etc/ssh/log/watch.log
