#ifndef ABFSTRUCT_H_INCLUDED
#define ABFSTRUCT_H_INCLUDED
#include <string.h>

/******
adaptive Bloom Filter
自适应布隆过滤器
ABF通过更改hash函数个数来记录元素出现的次数
ABF基于BF，将k个哈希函数指向的位置1，
当k个位已经被置1时，ABF使用附加的第k+1个哈希函数计算散列值，
若k+1位也已经被置1，ABF继续使用附加的哈希函数计算散列值，直到第K+N+1个位值为0，N即该元素出现的次数
****/
typedef struct AdaptiveBloomFilter{
    unsigned short * DictHash;
    int ABF_Bit_Count;
    int ABF_Hash_Num;
}AdBloomFilter;

AdBloomFilter ABF;

int InitABF()
{
    printf("Please enter Bloom Filter size:");
    scanf("%d",&ABF.ABF_Bit_Count);
    printf("Please enter the number of hash functions:");
    scanf("%d",&ABF.ABF_Hash_Num);
    //GetHashFunctionNum(ABF.ABF_Hash_Num);
    ABF.DictHash = (unsigned short*)malloc(ABF.ABF_Bit_Count*sizeof(unsigned short));
    if(ABF.DictHash)
        printf("Initializing successful!\n");
    else
        printf("Initialization failed!\n");
    for(int i = 0; i < ABF.ABF_Bit_Count; i++)
        ABF.DictHash[i] = 0;
    return 0;
}

int Destory()
{
    free(ABF.DictHash);
    return 0;
}

void Print()
{
    int i = 0;
    for(i = 0; i < ABF.ABF_Bit_Count; i++)
    {
        if(i % 10 == 0)
            printf("\n");
        printf("%d ",ABF.DictHash[i]);
    }
}


#endif // ABFSTRUCT_H_INCLUDED
