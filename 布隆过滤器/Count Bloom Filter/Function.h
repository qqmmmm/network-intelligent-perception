#ifndef FUNCTION_H_INCLUDED
#define FUNCTION_H_INCLUDED


int InitCBF();
int Destory();
int Insert(char * buffer);
int Delete(char * buffer);
int Query(char *buffer);
int ReadFile();
void Print();
void GetHashFunctionNum(int num);
int CheckBit();
int Operation();
int hash1();
int hash2();
int hash3();
int hash4();
int hash5();
int hash6();
int hash7();
int hash8();
int hash9();
int hash10();
int hash11();
int hash12();
#endif // FUNCTION_H_INCLUDED
