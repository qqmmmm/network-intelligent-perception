#ifndef BFCLASS_H_INCLUDED
#define BFCLASS_H_INCLUDED
#define MAXLEN 64
#include "HashFuction.h"

typedef struct BloomFilter{
    unsigned short* dicthash;
    int BloomFilterBitCount;
    int HashNumber;
}BloomFilter;

BloomFilter BF;

int InitBF()
{
    printf("Please enter Bloom Filter size:");
    scanf("%d",&BF.BloomFilterBitCount);
    printf("Please enter the number of hash functions:");
    scanf("%d",&BF.HashNumber);
    GetHashFunctionNum(BF.HashNumber);
    BF.dicthash = (unsigned short*)malloc(BF.BloomFilterBitCount*sizeof(unsigned short));
    if(BF.dicthash!=NULL)
        printf("YES!\n");
    memset(BF.dicthash,0,BF.BloomFilterBitCount);
    return 0;
}

int Destory()
{
    free(BF.dicthash);
    return 0;
}

void PrintBFCount()
{
    for(int i=1;i<=BF.BloomFilterBitCount;i++){
        printf("%d ",BF.dicthash[i-1]);
        if(i%32==0)
            printf("\n");
    }

}

#endif // BFCLASS_H_INCLUDED
