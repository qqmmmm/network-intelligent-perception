#ifndef HASH_H_INCLUDED
#define HASH_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "BFClass.h"

int HashNum = 0;//控制使用的hash函数个数

void GetHashNum(int num)
{
   HashNum = num;
}
/***********************************************
	函数名称: bool hash1(char* buffer, unsigned int*& dicthash, int command=0)
	函数功能: 将字符串按照ascii码变为数字，并得到其积，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
bool hash1(char* buffer,unsigned short *dicthash, int command=0){
	unsigned int tmp = 1;
	while (*buffer != '\0')
	{
		tmp *= (int)(*buffer);
		tmp %= BloomFilter.BloomFilterBitCount;
		buffer++;
	}
	if (command == 1)
        dicthash[tmp%BloomFilter.BloomFilterBitCount] = 1;
	return CheckBit(tmp % BloomFilter.BloomFilterBitCount);

}
/***********************************************
	函数名称: bool hash2(char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，并得到其和，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
bool hash2(char* buffer, int command = 0) {
	unsigned int tmp = 0;
	while (*buffer != '\0')
	{
		tmp += (int)(*buffer);

		buffer++;
	}
	if (command == 1)
        dicthash[tmp%BloomFilter.BloomFilterBitCount] = 1;
	return CheckBit(tmp % BloomFilter.BloomFilterBitCount);
}
/***********************************************
	函数名称: bool hash3(char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，并得到平方根取整的和，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
bool hash3(char* buffer, int command = 0) {
	unsigned int tmp = 1;
	while (*buffer != '\0')
	{
		tmp *= (int)(sqrt((double)*buffer));

		buffer++;
	}
	if (command == 1)
        dicthash[tmp%BloomFilter.BloomFilterBitCount] = 1;
	return CheckBit(tmp % BloomFilter.BloomFilterBitCount);
}
/***********************************************
	函数名称: bool hash34char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，按+-+-。。。顺序依次加减，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
bool hash4(char* buffer, int command = 0) {
	unsigned int tmp = 0;
	unsigned int sign = 1;
	while (*buffer != '\0')
	{
		tmp += sign*(int)(*buffer);
		sign *= -1;
		buffer++;
	}
	if (command == 1)
        dicthash[tmp%BloomFilter.BloomFilterBitCount] = 1;
	return CheckBit(tmp % BloomFilter.BloomFilterBitCount);
}

bool CheckBit(int BloomFilterBitCount)
{
    if(BloomFilterBitCount == 1)
        return true;
    return false;
}
#endif // HASH_H_INCLUDED
