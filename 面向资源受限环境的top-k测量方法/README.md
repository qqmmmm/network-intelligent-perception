# MSLCFinder top-k measurement

#### 介绍

MSLCFinder是东南大学网络空间安全学院程光教授团队成果，该方法是一种面向资源受限情况下的top-k测量方案。

本方法基于多级抽样使用较小资源实现较大top-k的测量

实验结果参看参考文献：Dai X, Cheng G, Yu Z, et al. MSLCFinder: An Algorithm in Limited Resources Environment for Finding Top-k Elephant Flows[J]. Applied Sciences, 2023, 13(1): 575.

#### 软件架构

![System Overview](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230312101806.png)

![测量流程](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230312101928.png)


![top-k记录流程](image.png)


#### 数据集

CAIDA2018，CRTNET华东北节点采集流量等，下载地址：
https://github.com/ccie44899/MSCLFinder.git


#### 参考论文

Dai X, Cheng G, Yu Z, et al. MSLCFinder: An Algorithm in Limited Resources Environment for Finding Top-k Elephant Flows[J]. Applied Sciences, 2023, 13(1): 575.
