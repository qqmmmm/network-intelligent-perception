import sys
import random
def genFile(filename, block, size):
    f = open(filename, "ab")
    for i in range(block):
        print("current block count: ", i)
        content = bytes(b"")
        for j in range(size):
            c = chr(random.randint(0, 255)).encode('utf-8')
            content = content + c
        f.write(content)
 
if len(sys.argv) != 4:
    print("usage:\t" + sys.argv[0] + "\toutfilename\tblocknum\tsizenum")
    sys.exit(1)
genFile(sys.argv[1], eval(sys.argv[2]), eval(sys.argv[3]))
